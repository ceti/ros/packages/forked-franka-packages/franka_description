# Franka Description Package

Forked august 2020 and extended with adaption of applied forces.

Original repository: https://github.com/frankaemika/franka_ros/tree/kinetic-devel/franka_description

## How to install it

* navigate into the src-folder of your workspace
* create a folder with the name "franka_ros"
* cd into the franka_ros folder
* clone this repository inside the franka_ros folder


## What has changed

We modified the joint limits in the panda_arm.xacro.
More in detail they are increased/decreased by 0.1.
This had to be done to never let the cartesian planner hit the limits again.
The limits were always reached, as the planner includes them in his plannable value range.
